* Generate Entity from existing database
Step 1 :
php bin/console doctrine:mapping:import --force AppBundle xml

Error :
[Doctrine\DBAL\Exception\ConnectionException]
  An exception occured in driver: SQLSTATE[HY000] [2002] No such file or directory

Change database_host:localhost -> database_host:127.0.0.1

Step 2 :

> php bin/console doctrine:generate:entities AppBundle

Issue :
No Metadata Classes to process.

-> Should clear cache : > php bin/console cache:clear --env=dev
