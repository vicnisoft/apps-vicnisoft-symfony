<?php

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 4/12/16
 * Time: 5:59 PM
 */
class InvalidFormException extends \RuntimeException
{
    protected $form;

    public function __construct($message, $form = null)
    {
        parent::__construct($message);
        $this->form = $form;
    }

    /**
     * @return array|null
     */
    public function getForm()
    {
        return $this->form;
    }

}