<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 4/11/16
 * Time: 5:58 PM
 */

namespace AppBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class SecurityController extends Controller
{
    public function loginAction(Request $request){

        return $this->render('AppBundle:security:login.html.twig',
            array(
                'error' => '',
                'last_username' => 'admin',
            )
        );
    }

}