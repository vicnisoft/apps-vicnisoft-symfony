<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $users = $this->container->get('api.user.handler')->all(5, 0);

        // replace this example code with whatever you need
        return $this->render('AppBundle:default:default.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..'),
            'data' => $users,
        ]);
    }

    public function jspdfAction(Request $request){
        return $this->render('AppBundle:tests:jspdf.html.twig', [
            'data' => array(),
        ]);
    }
}
