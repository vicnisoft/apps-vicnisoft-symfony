<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ipAddress','text')
            ->add('username','text')
            ->add('password','text')
            ->add('email','text')
            ->add('activationCode','text')
            ->add('forgottenPasswordCode','text')
            ->add('forgottenPasswordTime','integer')
            ->add('rememberCode','text')
            ->add('createdOn','integer')
            ->add('lastLogin','integer')
            ->add('active','boolean')
            ->add('firstName','text')
            ->add('lastName','text')
            ->add('company','text')
            ->add('phone','text')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Users'
        ));
    }
}
