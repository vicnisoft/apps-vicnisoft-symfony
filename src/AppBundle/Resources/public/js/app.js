/**
 * Created by canngo on 4/11/16.
 */


var specialElementHandlers = {
    '#editor': function (element, renderer) {
        return true;
    }
};
$('#cmd').click(function () {

    pdfTable();
    //var doc = new jsPDF();
    //var width = $(window).width();
    //doc.fromHTML($('#content').html(), 15, 15, {
    //    'width': 5000,
    //    'elementHandlers': specialElementHandlers
    //});
    //doc.save('sample-file.pdf');
});

function pdfTable(){
    var doc = new jsPDF('p', 'pt');
    var elem = $("#content");
    var res = doc.autoTableHtmlToJson(elem);
    doc.autoTable(res.columns, res.data);
    doc.save("table.pdf");
}

function demoFromHTML() {
    var pdf = new jsPDF('p', 'pt', 'letter');
    // source can be HTML-formatted string, or a reference
    // to an actual DOM element from which the text will be scraped.
    source = $('#data-source')[0];

    // we support special element handlers. Register them with jQuery-style
    // ID selector for either ID or node name. ("#iAmID", "div", "span" etc.)
    // There is no support for any other type of selectors
    // (class, of compound) at this time.
    specialElementHandlers = {
        // element with id of "bypass" - jQuery style selector
        '#bypassme': function(element, renderer) {
            // true = "handled elsewhere, bypass text extraction"
            return true
        }
    };
    var width = $(window).width();
    margins = {
        top: 80,
        bottom: 60,
        left: 40,
        width: width
    };
    // all coords and widths are in jsPDF instance's declared units
    // 'inches' in this case
    pdf.fromHTML(
        source, // HTML string or DOM elem ref.
        margins.left, // x coord
        margins.top, {// y coord
            'width': margins.width, // max width of content on PDF
            'elementHandlers': specialElementHandlers
        },
        function(dispose) {
            // dispose: object with X, Y of the last line add to the PDF
            //          this allow the insertion of new lines after html
            pdf.save('Test.pdf');
        }
        , margins);
}