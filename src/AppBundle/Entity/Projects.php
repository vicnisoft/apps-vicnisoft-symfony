<?php

namespace AppBundle\Entity;

/**
 * Projects
 */
class Projects
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $createdOn;

    /**
     * @var string
     */
    private $projectIcon;

    /**
     * @var boolean
     */
    private $recycled;

    /**
     * @var integer
     */
    private $id;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return Projects
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Projects
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set createdOn
     *
     * @param integer $createdOn
     *
     * @return Projects
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return integer
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set projectIcon
     *
     * @param string $projectIcon
     *
     * @return Projects
     */
    public function setProjectIcon($projectIcon)
    {
        $this->projectIcon = $projectIcon;

        return $this;
    }

    /**
     * Get projectIcon
     *
     * @return string
     */
    public function getProjectIcon()
    {
        return $this->projectIcon;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     *
     * @return Projects
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
