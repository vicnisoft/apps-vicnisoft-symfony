<?php

namespace AppBundle\Handler;

/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 4/12/16
 * Time: 5:53 PM
 */

use AppBundle\Model\UserInterface;
use AppBundle\Form\UsersType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;

class UserHandler implements  UserHandlerInterface
{

    private $om;
    private $entityClass;
    private $repository;
    private $formFactory;

    public function __construct(EntityManager $om, $entityClass, FormFactoryInterface $formFactory)
    {

        // TODO: Implement __construct() method.
        $this->om = $om;
        $this->entityClass = $entityClass;
        $this->repository = $this->om->getRepository("AppBundle:Users");
        $this->formFactory = $formFactory;
    }

    /**
     * Get a User.
     *
     * @param mixed $id
     *
     * @return UserInterface
     */
    public function get($id)
    {
        // TODO: Implement get() method.
        return $this->repository->find($id);

    }

    /**
     * Get a list of Users.
     *
     * @param int $limit the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0)
    {
        // TODO: Implement all() method.
        return $this->repository->findBy(array(), null, $limit, $offset);

    }

    /**
     * Create a new User.
     *
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function post(array $parameters)
    {
        // TODO: Implement post() method.
        $user = $this->createUser();
        return $this->processForm($user,$parameters,"POST");

    }

    /**
     * Edit a User.
     *
     * @param UserInterface $user
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function put(UserInterface $user, array $parameters)
    {
        // TODO: Implement put() method.
        return $this->processForm($user,$parameters,"PUT");
    }

    /**
     * Partially update a user.
     *
     * @param UserInterface $user
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function patch(UserInterface $user, array $parameters)
    {
        // TODO: Implement patch() method.
        return $this->processForm($user,$parameters,"PATCH");

    }

    /**
     * Processes the form.
     *
     * @param UserInterface $user
     * @param array $parameters
     * @param String $method
     *
     * @return UserInterface
     *
     * @throws AppBundle\Exception\InvalidFormException
     */
    private function processForm(UserInterface $user, array $parameters, $method = "PUT")
    {
        // TODO: Implement processForm() method.
        $form = $this->formFactory->create(new UsersType(), $user, array('method' => $method));
        $form->submit($parameters, 'PATCH' !== $method);
        if ($form->isValid()) {

            $user = $form->getData();
            $this->om->persist($user);
            $this->om->flush($user);

            return $user;
        }


        throw new InvalidFormException('Invalid submitted data', $form);

    }

    private function createUser()
    {
        // TODO: Implement createUser() method.
        return new $this->entityClass();

    }
}