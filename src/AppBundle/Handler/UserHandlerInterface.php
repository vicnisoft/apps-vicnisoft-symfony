<?php

namespace AppBundle\Handler;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use AppBundle\Model\UserInterface;
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 4/12/16
 * Time: 5:54 PM
 */
interface UserHandlerInterface
{
//    private $om;
//    private $entityClass;
//    private $repository;
//    private $formFactory;

    public function __construct(EntityManager $om, $entityClass, FormFactoryInterface $formFactory);
//    {
//        $this->om = $om;
//        $this->entityClass = $entityClass;
//        $this->repository = $this->om->getRepository($this->entityClass);
//        $this->formFactory = $formFactory;
//    }

    /**
     * Get a User.
     *
     * @param mixed $id
     *
     * @return UserInterface
     */
    public function get($id);
//    {
//        return $this->repository->find($id);
//    }

    /**
     * Get a list of Users.
     *
     * @param int $limit  the limit of the result
     * @param int $offset starting from the offset
     *
     * @return array
     */
    public function all($limit = 5, $offset = 0);
//    {
//        return $this->repository->findBy(array(), null, $limit, $offset);
//    }

    /**
     * Create a new User.
     *
     * @param array $parameters
     *
     * @return UserInterface
     */
    public function post(array $parameters);
//    {
//        $user = $this->createUser();
//
//        return $this->processForm($user, $parameters, 'POST');
//    }

    /**
     * Edit a User.
     *
     * @param UserInterface $user
     * @param array         $parameters
     *
     * @return UserInterface
     */
    public function put(UserInterface $user, array $parameters);
//    {
//        return $this->processForm($user, $parameters, 'PUT');
//    }

    /**
     * Partially update a user.
     *
     * @param UserInterface $user
     * @param array         $parameters
     *
     * @return UserInterface
     */
    public function patch(UserInterface $user, array $parameters);
//    {
//        return $this->processForm($user, $parameters, 'PATCH');
//    }



}