<?php

namespace Vns\AppsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UsersType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('ipAddress')
            ->add('username')
            ->add('password')
            ->add('salt')
            ->add('email')
            ->add('activationCode')
            ->add('forgottenPasswordCode')
            ->add('forgottenPasswordTime')
            ->add('rememberCode')
            ->add('createdOn')
            ->add('lastLogin')
            ->add('active')
            ->add('firstName')
            ->add('lastName')
            ->add('company')
            ->add('phone')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Vns\AppsBundle\Entity\Users'
        ));
    }
}
