<?php
/**
 * Created by PhpStorm.
 * User: canngo
 * Date: 4/12/16
 * Time: 3:54 PM
 */

namespace Vns\AppsBundle\Controller;


use FOS\RestBundle\Controller\FOSRestController;

class RestApiController extends FOSRestController
{
    public function getUserAction($id){
        return $this->getDoctrine()->getRepository('VnsAppsBundle:Users')->find($id);
    }
}