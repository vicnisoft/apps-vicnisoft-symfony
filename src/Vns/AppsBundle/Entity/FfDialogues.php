<?php

namespace Vns\AppsBundle\Entity;

/**
 * FfDialogues
 */
class FfDialogues
{
    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $content;

    /**
     * @var string
     */
    private $iconUrl;

    /**
     * @var string
     */
    private $audioUrl;

    /**
     * @var integer
     */
    private $createdOn;

    /**
     * @var boolean
     */
    private $recycled = '0';

    /**
     * @var integer
     */
    private $updatedOn;

    /**
     * @var boolean
     */
    private $approved = '0';

    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Vns\AppsBundle\Entity\FfCategories
     */
    private $category;

    /**
     * @var \Vns\AppsBundle\Entity\Users
     */
    private $createdBy;

    /**
     * @var \Vns\AppsBundle\Entity\Users
     */
    private $inspector;

    /**
     * @var \Vns\AppsBundle\Entity\Users
     */
    private $updatedBy;


    /**
     * Set name
     *
     * @param string $name
     *
     * @return FfDialogues
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return FfDialogues
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set iconUrl
     *
     * @param string $iconUrl
     *
     * @return FfDialogues
     */
    public function setIconUrl($iconUrl)
    {
        $this->iconUrl = $iconUrl;

        return $this;
    }

    /**
     * Get iconUrl
     *
     * @return string
     */
    public function getIconUrl()
    {
        return $this->iconUrl;
    }

    /**
     * Set audioUrl
     *
     * @param string $audioUrl
     *
     * @return FfDialogues
     */
    public function setAudioUrl($audioUrl)
    {
        $this->audioUrl = $audioUrl;

        return $this;
    }

    /**
     * Get audioUrl
     *
     * @return string
     */
    public function getAudioUrl()
    {
        return $this->audioUrl;
    }

    /**
     * Set createdOn
     *
     * @param integer $createdOn
     *
     * @return FfDialogues
     */
    public function setCreatedOn($createdOn)
    {
        $this->createdOn = $createdOn;

        return $this;
    }

    /**
     * Get createdOn
     *
     * @return integer
     */
    public function getCreatedOn()
    {
        return $this->createdOn;
    }

    /**
     * Set recycled
     *
     * @param boolean $recycled
     *
     * @return FfDialogues
     */
    public function setRecycled($recycled)
    {
        $this->recycled = $recycled;

        return $this;
    }

    /**
     * Get recycled
     *
     * @return boolean
     */
    public function getRecycled()
    {
        return $this->recycled;
    }

    /**
     * Set updatedOn
     *
     * @param integer $updatedOn
     *
     * @return FfDialogues
     */
    public function setUpdatedOn($updatedOn)
    {
        $this->updatedOn = $updatedOn;

        return $this;
    }

    /**
     * Get updatedOn
     *
     * @return integer
     */
    public function getUpdatedOn()
    {
        return $this->updatedOn;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     *
     * @return FfDialogues
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param \Vns\AppsBundle\Entity\FfCategories $category
     *
     * @return FfDialogues
     */
    public function setCategory(\Vns\AppsBundle\Entity\FfCategories $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \Vns\AppsBundle\Entity\FfCategories
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set createdBy
     *
     * @param \Vns\AppsBundle\Entity\Users $createdBy
     *
     * @return FfDialogues
     */
    public function setCreatedBy(\Vns\AppsBundle\Entity\Users $createdBy = null)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return \Vns\AppsBundle\Entity\Users
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * Set inspector
     *
     * @param \Vns\AppsBundle\Entity\Users $inspector
     *
     * @return FfDialogues
     */
    public function setInspector(\Vns\AppsBundle\Entity\Users $inspector = null)
    {
        $this->inspector = $inspector;

        return $this;
    }

    /**
     * Get inspector
     *
     * @return \Vns\AppsBundle\Entity\Users
     */
    public function getInspector()
    {
        return $this->inspector;
    }

    /**
     * Set updatedBy
     *
     * @param \Vns\AppsBundle\Entity\Users $updatedBy
     *
     * @return FfDialogues
     */
    public function setUpdatedBy(\Vns\AppsBundle\Entity\Users $updatedBy = null)
    {
        $this->updatedBy = $updatedBy;

        return $this;
    }

    /**
     * Get updatedBy
     *
     * @return \Vns\AppsBundle\Entity\Users
     */
    public function getUpdatedBy()
    {
        return $this->updatedBy;
    }
}

