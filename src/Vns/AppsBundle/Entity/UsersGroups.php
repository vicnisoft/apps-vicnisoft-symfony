<?php

namespace Vns\AppsBundle\Entity;

/**
 * UsersGroups
 */
class UsersGroups
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \Vns\AppsBundle\Entity\Groups
     */
    private $group;

    /**
     * @var \Vns\AppsBundle\Entity\Users
     */
    private $user;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set group
     *
     * @param \Vns\AppsBundle\Entity\Groups $group
     *
     * @return UsersGroups
     */
    public function setGroup(\Vns\AppsBundle\Entity\Groups $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return \Vns\AppsBundle\Entity\Groups
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * Set user
     *
     * @param \Vns\AppsBundle\Entity\Users $user
     *
     * @return UsersGroups
     */
    public function setUser(\Vns\AppsBundle\Entity\Users $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Vns\AppsBundle\Entity\Users
     */
    public function getUser()
    {
        return $this->user;
    }
}

