<?php

/* @Twig/Exception/traces.txt.twig */
class __TwigTemplate_fd8a16c6559329f855509f964a989c08812e6ce1b6feb5e7a151102ce37464d4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a1ea3b991b1c1c5d3f7f903cdbb68d1dfc60a61ad5b9d2b593e605457a54087 = $this->env->getExtension("native_profiler");
        $__internal_0a1ea3b991b1c1c5d3f7f903cdbb68d1dfc60a61ad5b9d2b593e605457a54087->enter($__internal_0a1ea3b991b1c1c5d3f7f903cdbb68d1dfc60a61ad5b9d2b593e605457a54087_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/traces.txt.twig"));

        // line 1
        if (twig_length_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()))) {
            // line 2
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "trace", array()));
            foreach ($context['_seq'] as $context["_key"] => $context["trace"]) {
                // line 3
                $this->loadTemplate("@Twig/Exception/trace.txt.twig", "@Twig/Exception/traces.txt.twig", 3)->display(array("trace" => $context["trace"]));
                // line 4
                echo "
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trace'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        
        $__internal_0a1ea3b991b1c1c5d3f7f903cdbb68d1dfc60a61ad5b9d2b593e605457a54087->leave($__internal_0a1ea3b991b1c1c5d3f7f903cdbb68d1dfc60a61ad5b9d2b593e605457a54087_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/traces.txt.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  30 => 4,  28 => 3,  24 => 2,  22 => 1,);
    }
}
/* {% if exception.trace|length %}*/
/* {% for trace in exception.trace %}*/
/* {% include '@Twig/Exception/trace.txt.twig' with { 'trace': trace } only %}*/
/* */
/* {% endfor %}*/
/* {% endif %}*/
/* */
