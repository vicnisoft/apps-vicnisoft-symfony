<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_e670db2f5b9e5373f1381dadbaaede7af1a0c1ae82f3b84147596b3eccd72730 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_56affc4138217be32a71792ba7bc0d2cf7c23be2bdbb95b4cf8c04135ab506ff = $this->env->getExtension("native_profiler");
        $__internal_56affc4138217be32a71792ba7bc0d2cf7c23be2bdbb95b4cf8c04135ab506ff->enter($__internal_56affc4138217be32a71792ba7bc0d2cf7c23be2bdbb95b4cf8c04135ab506ff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_56affc4138217be32a71792ba7bc0d2cf7c23be2bdbb95b4cf8c04135ab506ff->leave($__internal_56affc4138217be32a71792ba7bc0d2cf7c23be2bdbb95b4cf8c04135ab506ff_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5608b9135834fa1a2cd1405cc7b557485ef39dfdbc7aa403c5471ad778bb5e2a = $this->env->getExtension("native_profiler");
        $__internal_5608b9135834fa1a2cd1405cc7b557485ef39dfdbc7aa403c5471ad778bb5e2a->enter($__internal_5608b9135834fa1a2cd1405cc7b557485ef39dfdbc7aa403c5471ad778bb5e2a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_5608b9135834fa1a2cd1405cc7b557485ef39dfdbc7aa403c5471ad778bb5e2a->leave($__internal_5608b9135834fa1a2cd1405cc7b557485ef39dfdbc7aa403c5471ad778bb5e2a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_5e98c0e01ee1d953aa036d60cde17977d8ea5259d733e1066e260194f67fc8e4 = $this->env->getExtension("native_profiler");
        $__internal_5e98c0e01ee1d953aa036d60cde17977d8ea5259d733e1066e260194f67fc8e4->enter($__internal_5e98c0e01ee1d953aa036d60cde17977d8ea5259d733e1066e260194f67fc8e4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_5e98c0e01ee1d953aa036d60cde17977d8ea5259d733e1066e260194f67fc8e4->leave($__internal_5e98c0e01ee1d953aa036d60cde17977d8ea5259d733e1066e260194f67fc8e4_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_82cf3942331b3f3031d02cc2365446845c698500c9ab6a96c210dc53ef21ddf9 = $this->env->getExtension("native_profiler");
        $__internal_82cf3942331b3f3031d02cc2365446845c698500c9ab6a96c210dc53ef21ddf9->enter($__internal_82cf3942331b3f3031d02cc2365446845c698500c9ab6a96c210dc53ef21ddf9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_82cf3942331b3f3031d02cc2365446845c698500c9ab6a96c210dc53ef21ddf9->leave($__internal_82cf3942331b3f3031d02cc2365446845c698500c9ab6a96c210dc53ef21ddf9_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@Twig/layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include '@Twig/Exception/exception.html.twig' %}*/
/* {% endblock %}*/
/* */
