<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_35bd54bbc0fb21d64f3d94d963ac520b4fd522a308abb03fd7cc7aed69b9055b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f582c49c04e4528f853af1637ea443dc3945f17eac20dfcedf49253e767101de = $this->env->getExtension("native_profiler");
        $__internal_f582c49c04e4528f853af1637ea443dc3945f17eac20dfcedf49253e767101de->enter($__internal_f582c49c04e4528f853af1637ea443dc3945f17eac20dfcedf49253e767101de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f582c49c04e4528f853af1637ea443dc3945f17eac20dfcedf49253e767101de->leave($__internal_f582c49c04e4528f853af1637ea443dc3945f17eac20dfcedf49253e767101de_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_4e3db0df6f03fd48afd63696a6409add09c74956eb7aeb291a043bf64dd22fe6 = $this->env->getExtension("native_profiler");
        $__internal_4e3db0df6f03fd48afd63696a6409add09c74956eb7aeb291a043bf64dd22fe6->enter($__internal_4e3db0df6f03fd48afd63696a6409add09c74956eb7aeb291a043bf64dd22fe6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4e3db0df6f03fd48afd63696a6409add09c74956eb7aeb291a043bf64dd22fe6->leave($__internal_4e3db0df6f03fd48afd63696a6409add09c74956eb7aeb291a043bf64dd22fe6_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_ee93f6d25c9ce414898154773b388b8dd6c0acfe744560ac68947e9b386b2ace = $this->env->getExtension("native_profiler");
        $__internal_ee93f6d25c9ce414898154773b388b8dd6c0acfe744560ac68947e9b386b2ace->enter($__internal_ee93f6d25c9ce414898154773b388b8dd6c0acfe744560ac68947e9b386b2ace_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_ee93f6d25c9ce414898154773b388b8dd6c0acfe744560ac68947e9b386b2ace->leave($__internal_ee93f6d25c9ce414898154773b388b8dd6c0acfe744560ac68947e9b386b2ace_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0986853b945f0c6c70be32399926511716494cc92f08f2879d39c1cb2f64cd14 = $this->env->getExtension("native_profiler");
        $__internal_0986853b945f0c6c70be32399926511716494cc92f08f2879d39c1cb2f64cd14->enter($__internal_0986853b945f0c6c70be32399926511716494cc92f08f2879d39c1cb2f64cd14_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('http_kernel')->renderFragment($this->env->getExtension('routing')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_0986853b945f0c6c70be32399926511716494cc92f08f2879d39c1cb2f64cd14->leave($__internal_0986853b945f0c6c70be32399926511716494cc92f08f2879d39c1cb2f64cd14_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }
}
/* {% extends '@WebProfiler/Profiler/layout.html.twig' %}*/
/* */
/* {% block toolbar %}{% endblock %}*/
/* */
/* {% block menu %}*/
/* <span class="label">*/
/*     <span class="icon">{{ include('@WebProfiler/Icon/router.svg') }}</span>*/
/*     <strong>Routing</strong>*/
/* </span>*/
/* {% endblock %}*/
/* */
/* {% block panel %}*/
/*     {{ render(path('_profiler_router', { token: token })) }}*/
/* {% endblock %}*/
/* */
